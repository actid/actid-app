from django.shortcuts import render, HttpResponseRedirect, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import JsonResponse
import textgrids, json

from django.contrib.auth.models import Group, User
from .models import Etudiant, Groupe, Universite, Traduction
from .forms import FormEtudiant, FormGroupe, FormTraduction, UserLoginForm
from .decorators import unauthenticated_user, allowed_users

def home(request):
    return render(request, 'home.html')

##### VIEWS COMMUNES
@login_required
def delInstancePage(request,ClassName,instId):
    inst = eval(ClassName).objects.get(id=instId)
    return render(request, 'deleteInstance.html', {"ClassName":ClassName, "className":ClassName.lower(), "inst":inst})

@login_required
def delInstance(request,ClassName,instId):
    inst = eval(ClassName).objects.get(id=instId)
    if request.method == 'POST':
        inst.delete()
        context = {
            "messages": [
                { "message":"L'instance a été supprimée.", "tag":"success"}
            ]
        }
        return HttpResponseRedirect('/'+ClassName.lower()+'s/', context)

##### ETUDIANTS
def etudiants(request):
    etudiants = Etudiant.objects.all()
    etu2trds = {}
    for etu in etudiants:
        trads = Traduction.objects.filter(Q(traducteurs=etu.id) | Q(correcteurs=etu.id))
        etu2trds[etu.id] = [tr.id for tr in trads]
    return render(request, 'etudiants.html', {"etudiants":etudiants, "etu2trds":etu2trds})

def showEtudiant(request, etuId):
    etu = Etudiant.objects.get(id=etuId)
    traductions = []
    for tr in Traduction.objects.filter(traducteurs=etuId):
        traductions.append(tr.id)    
    for tr in Traduction.objects.filter(correcteurs=etuId):
        traductions.append(tr.id)
    print(traductions)
    return render(request, 'showEtudiant.html', {'etudiant':etu, 'traductions':traductions})

@login_required
def addEtudiant(request):
    if request.method == 'POST':
        form = FormEtudiant(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/showEtudiant/'+form.cleaned_data['id']+'/')
    else:
        form = FormEtudiant()
    return render(request, 'editEtudiant.html', {'form': form, 'edit':False})

@login_required
def editEtudiant(request, etuId):
    etu = Etudiant.objects.get(id=etuId)
    if request.method == 'POST':
        form = FormEtudiant(request.POST, instance=etu)
        if form.is_valid():
            form.save()
            context = {
                "messages": [
                    { "message":"Bien enregistré ! :)", "tag":"success"}
                ]
            }
            return HttpResponseRedirect('/showEtudiant/'+form.cleaned_data['id']+'/', context)
    else:
        form = FormEtudiant(instance=etu)
    return render(request, 'editEtudiant.html', {'form': form, 'edit':True, 'etu':etuId})


##### GROUPES
def groupes(request):
    groupes = Groupe.objects.all()
    grp2etus = {}
    for groupe in groupes:
        etudiants = Etudiant.objects.filter(groupe=groupe.id)
        grp2etus[groupe.id] = [etu.id for etu in etudiants]
    return render(request, 'groupes.html', {"groupes":groupes, "grp2etus":grp2etus})

def showGroupe(request, grpId):
    grp = Groupe.objects.get(id=grpId)
    etudiants = Etudiant.objects.filter(groupe=grpId)
    traductions = Traduction.objects.filter(groupe=grpId)
    return render(request, 'showGroupe.html', {'groupe':grp, 'etudiants':etudiants, 'traductions':traductions})

@login_required
def addGroupe(request):
    if request.method == 'POST':
        form = FormGroupe(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/showGroupe/'+form.cleaned_data['id']+'/')
    else:
        form = FormGroupe()
    return render(request, 'editGroupe.html', {'form': form, 'edit':False})

@login_required
def editGroupe(request,grpId):
    grp = Groupe.objects.get(id=grpId)
    if request.method == 'POST':
        form = FormGroupe(request.POST, instance=grp)
        if form.is_valid():
            form.save()
            context = {
                "messages": [
                    { "message":"Bien enregistré ! :)", "tag":"success"}
                ]
            }
            return HttpResponseRedirect('/showGroupe/'+form.cleaned_data['id']+'/', context)
    else:
        form = FormGroupe(instance=grp)
    return render(request, 'editGroupe.html', {'form': form, 'edit':True, 'grp':grpId})


##### TRADUCTIONS
def traductions(request):
    traductions = Traduction.objects.all()
    return render(request, 'traductions.html', {"traductions":traductions})

def showTraduction(request, trdId):
    trd = Traduction.objects.get(id=trdId)
    fileCats = ['source', 'initial', 'revision', 'final']
    return render(request, 'showTraduction.html', {'traduction':trd, 'fileCats':fileCats})

@login_required
def addTraduction(request):
    if request.method == 'POST':
        form = FormTraduction(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/showTraduction/'+form.cleaned_data['id']+'/')
    else:
        form = FormTraduction()
    return render(request, 'editTraduction.html', {'form': form, 'edit':False})

@login_required
def editTraduction(request,trdId):
    trd = Traduction.objects.get(id=trdId)
    if request.method == 'POST':
        form = FormTraduction(request.POST, instance=trd)
        if form.is_valid():
            form.save()
            context = {
                "messages": [
                    { "message":"Bien enregistré ! :)", "tag":"success"}
                ]
            }
            return HttpResponseRedirect('/showTraduction/'+form.cleaned_data['id']+'/', context)
    else:
        form = FormTraduction(instance=trd)
    return render(request, 'editTraduction.html', {'form': form, 'edit':True, 'trd':trdId})


@login_required
def corpusViewer(request,trdId):
    with open("static/corpus/traductions/"+trdId+"_source.xml", 'r') as infile:
        xmls = infile.read()

    with open("static/corpus/traductions/"+trdId+"_initial.xml", 'r') as infile:
        xmli = infile.read()

    with open("static/corpus/traductions/"+trdId+"_revision.xml", 'r') as infile:
        xmlr = infile.read()

    with open("static/corpus/traductions/"+trdId+"_final.xml", 'r') as infile:
        xmlf = infile.read()

    with open("static/corpus/traductions/"+trdId+"_info.xml", 'r') as infile:
        xmlinfo = infile.read()

    trd = Traduction.objects.get(id=trdId)
    etus = {
        'traducteurs': [ e.id for e in trd.traducteurs.all() ],
        'correcteurs': [ e.id for e in trd.correcteurs.all() ]
    }

    data = {
        'xmls': xmls,
        'xmli': xmli,
        'xmlr': xmlr,
        'xmlf': xmlf,
        'xmlinfo': xmlinfo,
        'traduction': trd,
        'etus': json.dumps(etus)
    }
    return render(request, 'corpusViewer.html', data)


def getElan(request):
    colis = json.loads(request.body)
    trdId = colis['trdId']
    p = colis['p']
    g = trdId.replace('frjp','').replace('jpfr','')

    # print(trdId,g,p)

    grid = textgrids.TextGrid('static/corpus/elan/'+g+'_zoom1.TextGrid')
    pmin = 0
    pmax = 0
    segs = [] # contiendra des { 'pints':[], 'video':'' }
    v = 0 # itérateur identifiant vidéo (1, ou 2 si deux vidéos pour le même p)

    for i in grid['Ref_phrase']:
        if i.text == trdId+"-p"+str(p):
            v += 1
            pmin = i.xmin
            pmax = i.xmax
            pints = [] # liste de (etu, langue, i.xmin, i.xmax, i.text, jp2fr)

            for tier in grid:
                t = tier.split('-')
                if len(t)==2:
                    etu, langue = t
                    if langue in ['fr', 'jp', 'jp2fr']: 
                        for i in grid[tier]:
                            if i.mid > pmin and i.mid < pmax and i.text != "":

                                if langue == 'jp2fr':
                                    for pint in pints:
                                        if pint[2] == i.xmin and pint[3] == i.xmax and pint[1] == "jp":
                                            pint[5] = i.text
                                else:
                                    pints.append([etu, langue, i.xmin, i.xmax, i.text, ""])

            seg = {
                'pints': sorted(pints,key=lambda t:t[2]),
                'video': trdId+'-p'+str(p)+'_'+str(v)+'.mp4',
                'offset': pmin-3
            }
            segs.append(seg)

    ## Envoi réponse au client
    rep = {
        "segs": segs
    }
    
    return JsonResponse(rep)


def recherche(request):    
    return render(request, 'recherche.html')


@unauthenticated_user
def loginView(request):
    nextPage = request.GET.get('next')
    print("NextPage=",nextPage)
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username = username, password = password)
        
        if user is not None:
            login(request, user)

            if nextPage:
                return redirect(nextPage)
            return redirect('/')
    
    context = {
        'form': form,
    }
    return render(request, 'users/login.html', context)