from django.db import models
from django.contrib.auth.models import User

class Langue(models.Model):
    langue = models.CharField(max_length=50,primary_key=True)
    def __str__(self):
        return self.langue

class Sexe(models.Model):
    sexe = models.CharField(max_length=1,primary_key=True)
    def __str__(self):
        return self.sexe

class Universite(models.Model):
    dateCreation = models.DateTimeField(auto_now_add=True, auto_now=False)
    dateModification = models.DateTimeField(auto_now_add=False, auto_now=True)
    nom = models.CharField(max_length=255)
    id = models.CharField(max_length=20,primary_key=True)

    def __str__(self):
        return self.id

class Groupe(models.Model):
    dateCreation = models.DateTimeField(auto_now_add=True, auto_now=False)
    dateModification = models.DateTimeField(auto_now_add=False, auto_now=True)
    id = models.CharField(max_length=50,primary_key=True)
    annee = models.CharField(max_length=10)
    
    def __str__(self):
        return self.id

class Etudiant(models.Model):
    dateCreation = models.DateTimeField(auto_now_add=True, auto_now=False)
    dateModification = models.DateTimeField(auto_now_add=False, auto_now=True)
    
    id = models.CharField(max_length=50,primary_key=True)
    langueMaternelle = models.ForeignKey('Langue', on_delete=models.PROTECT)
    sexe = models.ForeignKey('Sexe', on_delete=models.PROTECT)
    
    groupe = models.ForeignKey('Groupe', on_delete=models.PROTECT)
    universite = models.ForeignKey('Universite', on_delete=models.PROTECT)

    def __str__(self):
        return self.id

class Traduction(models.Model):
    id = models.CharField(max_length=50,primary_key=True)
    dateCreation = models.DateTimeField(auto_now_add=True, auto_now=False)
    dateModification = models.DateTimeField(auto_now_add=False, auto_now=True)
    langueSource = models.ForeignKey('Langue', on_delete=models.PROTECT, related_name='langueSource')
    langueCible = models.ForeignKey('Langue', on_delete=models.PROTECT, related_name='langueCible')
    groupe = models.ForeignKey('Groupe', on_delete=models.PROTECT)
    titre1 = models.CharField(max_length=255)
    titre2 = models.CharField(max_length=255)
    auteur = models.CharField(max_length=255)
    datePubli = models.DateField(auto_now_add=False, auto_now=False)
    source = models.CharField(max_length=255)
    traducteurs = models.ManyToManyField('Etudiant', related_name='traducteurs')
    correcteurs = models.ManyToManyField('Etudiant', related_name='correcteurs')
    
    def __str__(self):
        return self.id

class Annotation(models.Model):
    id = models.CharField(max_length=20,primary_key=True)
    date = models.DateTimeField(auto_now_add=False, auto_now=False)
    dateModification = models.DateTimeField(auto_now_add=False, auto_now=True)
    auteur = models.ForeignKey('Etudiant', on_delete=models.PROTECT)
    fichier = models.CharField(max_length=50, default="") # ex. g2jpfr-p0

    typeRevision = models.ForeignKey('TypeRevision', on_delete=models.SET_DEFAULT, default="")
    resolu = models.BooleanField(default=False)
    comment = models.CharField(max_length=255, default="")
    initial = models.CharField(max_length=255, default="")
    final = models.CharField(max_length=255, default="")

    pos_i = models.CharField(max_length=255, default="[]")
    pos_f = models.CharField(max_length=255, default="[]")
    pos = models.CharField(max_length=255, default="[]")
    tags = models.CharField(max_length=255, default="[]")

    typeErreur = models.ForeignKey('TypeErreur', on_delete=models.SET_DEFAULT, default="")

class TypeErreur(models.Model):
    id = models.CharField(max_length=20,primary_key=True)
    nom = models.CharField(max_length=50)

class TypeRevision(models.Model):
    id = models.CharField(max_length=20,primary_key=True)
    nom = models.CharField(max_length=50)