from django.contrib import admin
from .models import Universite, Groupe, Etudiant, Traduction, Langue, Sexe, Annotation, TypeRevision, TypeErreur

# TUTO : https://www.youtube.com/watch?v=g5DTIiFAiSk 
admin.site.site_header = 'Tableau de bord de Corpolan'

admin.site.register(Universite)
admin.site.register(Groupe)
admin.site.register(Etudiant)
admin.site.register(Traduction)
admin.site.register(Langue)
admin.site.register(Sexe)

admin.site.register(Annotation)
admin.site.register(TypeRevision)
admin.site.register(TypeErreur)