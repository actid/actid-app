from django.apps import AppConfig


class CorpolanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'corpolan'
