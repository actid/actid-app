from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Etudiant, Groupe, Universite, Traduction


# https://docs.djangoproject.com/en/4.0/topics/forms/modelforms/

class FormEtudiant(ModelForm):
    class Meta:
        model = Etudiant
        fields = ['id','langueMaternelle','sexe','groupe','universite']
        labels = {
            'id':'Id',
            'langueMaternelle':'Langue maternelle',
            'sexe':'Sexe',
            'groupe':'Groupe',
            'universite':'Université'
        }

class FormGroupe(ModelForm):
    class Meta:
        model = Groupe
        fields = ['id','annee']

class FormTraduction(ModelForm):
    class Meta:
        model = Traduction
        fields = ['id','langueSource','langueCible','groupe','titre1','titre2','auteur','datePubli','source','traducteurs','correcteurs']


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        
    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username = username, password = password)

            if not user:
                raise forms.ValidationError("Ce nom d'utilisateur n'existe pas !")
            
            if not user.check_password(password):
                raise forms.ValidationError("Ce mot de passe est incorrect !")
        
        return super(UserLoginForm, self).clean(*args, **kwargs)