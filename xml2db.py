import xml.etree.ElementTree as ET
import sys, pymongo


########################################
########## CONNEXION MONGODB ###########
########################################
def connexion():
    mdp = ""
    with open('../dbmdp_actid','r') as infile:
        mdp = infile.read()
    print('Connexion à AlemDic...')
    
    mongodb_client = pymongo.MongoClient("mongodb+srv://actidapp:"+mdp+"@corpolan.vqoi2.mongodb.net/?retryWrites=true&w=majority")
    print(mongodb_client.list_database_names())
    return mongodb_client

dbc = connexion()
# db = dbc['traduction']


# tree = ET.parse(sys.argv[1])
# root = tree.getroot()

# for el in root.iter('comment'):
#     print(el)


# <comment id="0000" res="false" aut="g2ETU1" date="2020-12-08T14:12:00" text="たる">の</comment>
# <edit type="substitution" id="0000" resolu="1" aut="g2ETU1" date="" comment="" initial="que c’est," final="qu’une" tags="">que c’est,</edit>
# <edit type="deletion" id="0014" aut="g2ETU1" date="2021-01-27T12:45:00">の</edit>
# <edit type="deletion" id="0495" aut="g1ETU1" date="2020-01-24T21:46:00" first-type="insertion" first-aut="g1ETU2" first-date="2020-01-13T11:59:00">au contraire,</edit>