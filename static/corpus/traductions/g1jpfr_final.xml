<?xml version="1.0" encoding="UTF-8"?>
<DOCUMENT id="g1jpfr_initial">
    <TITRE id="titre0">
        <P id="p0">Les réseaux sociaux vous privent-ils de votre bonheur ? Le piège de la théorie établie concernant « le bonheur »</P>
    </TITRE>
        
    <TEXTE>
        <PAR id="par1">
            <P id="p1">Est-ce que le bonheur dont tout le monde parle correspond réellement à votre bonheur ?</P>
            <P id="p2">De nos jours, les réseaux sociaux répandent et divinisent une image déformée du bonheur : le bonheur c’est comme ça, un point c’est tout.</P>
            <P id="p3">Un scientifique anglais du comportement souligne l’importance de vivre sans être égaré par cette image du bonheur déformée.</P>
        </PAR>
        <PAR id="par2">
            <P id="p4">« Voulez-vous être heureux ? Alors, ne devenez pas jardinier. »</P>
        </PAR>
        <PAR id="par3">
            <P id="p5">Il semble que c’est ce que l’on dit, d’après une logique tordue.</P>
            <P id="p6">Quand j’avais 14 ans, une conseillère d’orientation est venue en classe nous poser une question : parmi vous, y a-t-il des personnes qui ont déjà décidé quel métier choisir dans le futur ?</P>
            <P id="p7"></P>
        </PAR>
        <PAR id="par4">
            <P id="p8">Un garçon a répondu, l’air sérieux : « Je voudrais devenir jardinier. »</P>
            <P id="p9"></P>
        </PAR>
        <PAR id="par5">
            <P id="p10">La conseillère a ri et lui a demandé s’il plaisantait.</P>
            <P id="p11">En se rendant compte que ses faits et dires étaient crûment mauvais, elle a alors posé une question à un autre élève avec précipitation.</P>
        </PAR>
        <PAR id="par6">
            <P id="p12">Il est évident que sa réponse était indigne d’une conseillère d’orientation.</P>
            <P id="p13">Pourtant, sa réaction reflète l’ordre des valeurs que la société donne à de diverses professions.</P>
            <P id="p14">Autrement dit, si on voulait devenir médecin, avocat ou homme d’affaires, elle dirait « Bien, c’est un métier admirable.</P>
            <P id="p15">Une vie heureuse t’attend. »</P>
        </PAR>
        <TITRE id="titre1" niveau="2">
            <P id="p16">Avocat ou fleuriste, lequel des deux est heureux ?</P>
        </TITRE>
        <PAR id="par7">
            <P id="p17">En revanche, Paul Dolan, un professeur anglais de sciences du comportement à la London school of Economics, dit que « ce n’est pas forcément vrai ».</P>
            <P id="p18">Il a publié en janvier 2019 « Happy Ever After : Escaping the Myth of the Perfect Life » qui remet en question la valeur du bonheur.</P>
        </PAR>
        <PAR id="par8">
            <P id="p19">« On a tendance à penser qu’il faut récompenser les gens qui poursuivent leur succès avec le désir de se perfectionner.</P>
            <P id="p20">Cependant, il faut aussi récompenser les gens qui réussissent avec des métiers qui ne sont pas généralement considérés comme admirables ».</P>
            <P id="p21">En principe, on a le droit d’être parfaitement heureux quelle que soit notre profession.</P>
        </PAR>
        <PAR id="par9">
            <P id="p22">Voyons voir les chiffres (montrant la réalité).</P>
            <P id="p23">Selon Dolan, 64% des avocats se sentent heureux.</P>
            <P id="p24">Vous pensez sûrement que ce taux est élevé.</P>
            <P id="p25">Néanmoins, le taux atteint 87% lorsque l’on demande la même chose aux fleuristes.</P>
        </PAR>
        <PAR id="par10">
            <P id="p26">Dolan s’attaque dans son livre à l’idée reçue que l’on ne peut devenir heureux que si l’on accède à un poste important.</P>
            <P id="p27">Cependant, ceci n’est qu’un simple exemple.</P>
            <P id="p28">Bien que son ouvrage puisse sembler n’être qu’une révélation personnelle qui appelle à se changer soi-même à travers de la pensée positive, l’essence de son ouvrage est bien une réelle inspection de spécialiste en science du comportement.</P>
        </PAR>
        <TITRE id="titre2" niveau="2">
            <P id="p29">Renverser « le mythe »</P>
        </TITRE>
        <PAR id="par11">
            <P id="p30">Cet ouvrage ouvre le débat non seulement sur la théorie que l’on ne peut devenir heureux que si l’on occupe un métier important, mais aussi sur le courant de pensée qui prône la monogamie et les mariages qui durent.</P>
            <P id="p31">De plus, il démolit toutes sortes de mythes, notamment le besoin d’avoir des enfants, d’aller à l’université, de gagner de l’argent, d’avoir une maison, de faire don à la charité (sans s’en vanter) ou encore d’ être en bonne santé pour être heureux.</P>
        </PAR>
        <PAR id="par12">
            <P id="p32">Certaines personnes sont peut-être choquées par le contraste avec les leçons enseignées jusqu’à présent.</P>
            <P id="p33">Pourtant, certaines études ont mis en évidence que le lien entre le thème mentionné ci-dessus et le sentiment de bonheur est extrêmement faible.</P>
        </PAR>
        <PAR id="par13">
            <P id="p34">Une enquête sur l’emploi du temps des Américains menée par le Bureau des statistiques du travail des États-Unis a rassemblé des données sur une durée de 10 ans, incluant le niveau de bonheur, de sens, de stress, de fatigue, de tristesse et de douleur.</P>
            <P id="p35">Lors de ses recherches, Dolan a trouvé que le niveau de bonheur des gens qui gagnent plus que 100 mille dollars par an était plus bas que celui de ceux qui gagnent moins que 25 mille dollars par an.</P>
        </PAR>
        <PAR id="par14">
            <P id="p36">D’après lui, 40% des étudiants d’anglais de l’université de Cambridge étaient diagnostiqués comme dépressifs en 2014.</P>
            <P id="p37">En outre, les personnes qui dépensent plus de 20 mille dollars pour leur mariage divorcent 2 fois plus que la moyenne tandis que les personnes qui dépensent moins de mille dollars pour leur mariage divorcent 2 fois moins.</P>
            <P id="p38">De plus, il réfute la théorie selon laquelle plus notre IMC (indicateur du degré d’obésité) est élevé, moins on serait heureux, affirmant qu’il n’a trouvé aucun lien entre les deux facteurs.</P>
        </PAR>
        <TITRE id="titre3" niveau="2">
            <P id="p39">Pourquoi les idées reçues sont nocives</P>
        </TITRE>
        <PAR id="par15">
            <P id="p40">On peut dire la même chose en ce qui concerne l’achat d’une maison.</P>
            <P id="p41">« Il est prouvé que le lien entre le fait d’avoir une maison et le niveau de bonheur est très faible.</P>
            <P id="p42">Pourtant, en Angleterre, l’idée que tout le monde doit avoir sa maison persévère ancrée. » explique Dolan.</P>
        </PAR>
        <PAR id="par16">
            <P id="p43">De plus, il fait remarquer que cette tendance tracasse la génération Y / puisqu’en Angleterre, le prix d’un bien immobilier s’élève à environ 10 fois le revenu annuel moyen, rendant presque impossible l’acquisition de sa propre maison.</P>
            <P id="p44"></P>
        </PAR>
        <PAR id="par17">
            <P id="p45">C’est la raison pour laquelle les idées reçues sont nocives.</P>
            <P id="p46">Plus on est pressé d’atteindre le but final que l’on s’impose, moins on est heureux lorsque l’on ne peut pas y arriver.</P>
            <P id="p47">Si on ne peut pas l’atteindre, il est possible qu’une conseillère d’orientation ricane de nous.</P>
        </PAR>
        <PAR id="par18">
            <P id="p48">« Nous jugeons bons ou mauvais les gens qui ne se conforment pas aux idées reçues, mais nous nous devons d’atténuer les inégalités dans notre jugement.»  commente Dolan.</P>
        </PAR>
        <TITRE id="titre4" niveau="2">
            <P id="p49">Nous pouvons changer la théorie établie</P>
        </TITRE>
        <PAR id="par19">
            <P id="p50">La différence entre le livre de Dolan et les livres de révélations personnelles qui prônent « le changement de soi à travers la pensée positive » est que son livre ne parle pas du bonheur individuel, mais propose de vivre au-dessus des idées reçues, sans se faire influencer par la dite valeur générale du bonheur.</P>
            <P id="p51"></P>
            <P id="p52"></P>
            <P id="p53">En faisant cela, nous ne jugeons plus les gens qui vivent de manière différente des idées reçues.</P>
        </PAR>
        <PAR id="par20">
            <P id="p54">Dolan ne conseille pas aux lecteurs de devenir « de bonnes personnes pleines de compassion », mais leur montre que la théorie généralement acceptée peut se renverser en changeant de logique.</P>
            <P id="p55"></P>
        </PAR>
        <PAR id="par21">
            <P id="p56">Prenons l’exemple de l’obésité :</P>
            <P id="p57">« À franchement parler, nous ne pensons pas vraiment du bien des personnes corpulentes » écrit-il.</P>
            <P id="p58">Alors que les frais médicaux pour les maladies liés à l’obésité du système de santé public anglais gonflent, l’Angleterre est devenue un des pays les plus touchés par l’obésité.</P>
            <P id="p59">L’obésité est incompatible avec notre idée du bonheur, car nous pensons qu’être en bonne santé est un des chemins vers le bonheur.</P>
            <P id="p60"></P>
        </PAR>
        <PAR id="par22">
            <P id="p61">Néanmoins, il souligne que les personnes en surpoids ont un risque élevé de mourir jeune.</P>
            <P id="p62"></P>
            <P id="p63">Autrement dit, puisqu’ils vivent moins longtemps, moins de taxes sont investies pour eux.</P>
            <P id="p64">On pourrait aussi dire qu’ils contribuent peut-être plus à l’économie car ils dépensent beaucoup en nourriture.</P>
        </PAR>
        <PAR id="par23">
            <P id="p65">Il en va de même pour les fumeurs.</P>
            <P id="p66">Bref, il n’y a pas de raison rationnelle pour juger ces gens.</P>
            <P id="p67">Le fait est que nous critiquons les personnes obèses et les fumeurs uniquement car ils adoptent des comportements que nous jugeons généralement mauvais.</P>
        </PAR>
        <TITRE id="titre5" niveau="2">
            <P id="p68">Il n’existe pas de règle pour tout mesurer</P>
        </TITRE>
        <PAR id="par24">
            <P id="p69">Dolan ne dit pas qu’il faut manger autant que l’on veut, ne pas avoir d’enfant ou ne pas aller à l’université.</P>
            <P id="p70">Il pense que réfléchir contre les idées reçues donne naissances à de nouvelles idées.</P>
        </PAR>
        <PAR id="par25">
            <P id="p71">Si une personne vit un marriage heureux avec un partenaire unique, a 5 enfants et habite dans une grande maison, c’est très bien comme ça aussi.</P>
            <P id="p72">Il veut seulement transmettre le message qu’ « une seule règle ne s’applique pas à tout. » </P>
            <P id="p73">Il considère son livre être « le manifeste de la société ».</P>
            <P id="p74">Il essaie de diriger nos yeux sur les éléments négatifs qui se cachent derrière l’idée toute faite du bonheur.</P>
        </PAR>
        <PAR id="par26">
            <P id="p75">Cela est particulièrement significatif dans ce monde où les réseaux sociaux approuvent et divinisent les idées reçues.</P>
            <P id="p76">« Les réseaux sociaux offrent à l’infini des occasions pour se comparer avec les gens qui revendiquent à haute voix les idées reçues » indique-t-il.</P>
        </PAR>
        <PAR id="par27">
            <P id="p77">Par exemple, lorsque quelqu’un poste sur Instagram des photos d’une cérémonie de mariage grandiose, une personne célibataire ou qui n’a pas d’argent se sent davantage misérable en voyant ces photos.</P>
            <P id="p78"></P>
        </PAR>
        <TITRE id="titre6" niveau="2">
            <P id="p79">Ce bonheur est-t-il mon bonheur ?</P>
        </TITRE>
        <PAR id="par28">
            <P id="p80">Au Canada, selon une recherche observant des gens qui ont gagné à la loterie et leurs relations, la probabilité que les voisins des gagneurs fassent faillite dans les 2 années suivantes était élevée.</P>
        </PAR>
        <PAR id="par29">
            <P id="p81">C’est parce que l’on est enclin à essayer de suivre le succès de notre entourage.</P>
            <P id="p82">À cette époque où on peut se connecter au monde entier avec un téléphone portable à travers les réseaux sociaux, on ne peut pas fermer les yeux sur ce qu’il en résulte.</P>
        </PAR>
        <PAR id="par30">
            <P id="p83">« Même si l’on devenait le deuxième plus riche au monde, on ne pourrait pas être satisfait tant qu’une personne supérieure existe. »</P>
            <P id="p84">Nos désirs ne s’épuisent jamais : gagner plus, être en relation avec plus de personnes, être mieux diplômé, devenir plus musclé...</P>
            <P id="p85"></P>
        </PAR>
        <PAR id="par31">
            <P id="p86">Dolan craint qu’un jour les réseaux sociaux tombent au même rang que les cigarettes dans une dizaine d’années.</P>
            <P id="p87">« Il faut prendre conscience que nous ne vivons peut-être que selon les idées reçues sur le bonheur et que notre bonheur peut se différencier de ces idées.»</P>
        </PAR>
        <PAR id="par32">
            <P id="p88">Je vous recommande de lire cet ouvrage en tant que premier pas dans la thérapie comportementale, ce qui vaut peut-être mieux que d’aller à la fameuse salle de gym alors que vous n’en avez même pas envie.</P>
            <P id="p89"></P>
        </PAR>
    </TEXTE>
</DOCUMENT>