///////////////////////////////////////
// JAVASCRIPT POUR LA PAGE VIEWER (visualisation du corpus)

// LISTE DES COULEURS POUR DISTINGUER ANNOTATIONS
let editcolor = ['lightblue','orange','yellow','cyan','white','lightcoral','lightblue','orange','yellow','cyan','white','purple'];
let colorAdd = 'lightgreen';
let colorDel = 'red';
let colorSub = 'orange';

// REMPLACEMENT DES BALISES XML → HTML
// <comment> → <span class="commentaire">
xmls = xmls.replace(/<comment/g, '<span class="commentaire"');
xmli = xmli.replace(/<comment/g, '<span class="commentaire"');
xmlr = xmlr.replace(/<comment/g, '<span class="commentaire"');
xmlf = xmlf.replace(/<comment/g, '<span class="commentaire"');

xmls = xmls.replace(/<\/comment>/g, '</span>');
xmli = xmli.replace(/<\/comment>/g, '</span>');
xmlr = xmlr.replace(/<\/comment>/g, '</span>');
xmlf = xmlf.replace(/<\/comment>/g, '</span>');

// <edit type="deleted"> → <span class="suppression">
xmls = xmls.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmli = xmli.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmlr = xmlr.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmlf = xmlf.replace(/<edit type="deleted"/g, '<span class="suppression"');

xmls = xmls.replace(/<\/edit>/g, '</span>');
xmli = xmli.replace(/<\/edit>/g, '</span>');
xmlr = xmlr.replace(/<\/edit>/g, '</span>');
xmlf = xmlf.replace(/<\/edit>/g, '</span>');

// SUIVI DE MODIFICATIONS WORD
// <edit id="ct94606708895024" type="deletion"
xmls = xmls.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmli = xmli.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmlr = xmlr.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmlf = xmlf.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');

// <edit id="ct94606708862640" type="insertion"
xmls = xmls.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmli = xmli.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmlr = xmlr.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmlf = xmlf.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');

// <edit type="substitution" idodf="idodf001"
xmls = xmls.replace(/<edit type="substitution"/g, '<span class="suiviModif substitution"');
xmli = xmli.replace(/<edit type="substitution"/g, '<span class="suiviModif substitution"');
xmlr = xmlr.replace(/<edit type="substitution"/g, '<span class="suiviModif substitution"');
xmlf = xmlf.replace(/<edit type="substitution"/g, '<span class="suiviModif substitution"');

if(window.DOMParser){
    parser = new DOMParser();
    doms = parser.parseFromString(xmls, "text/xml");
    domi = parser.parseFromString(xmli, "text/xml");
    domr = parser.parseFromString(xmlr, "text/xml");
    domf = parser.parseFromString(xmlf, "text/xml");
    dominfo = parser.parseFromString(xmlinfo, "text/xml");
}
console.log("Bonjour.");
document.getElementById('titre').innerHTML = doms.getElementsByTagName('TITRE')[0].innerHTML;
let groupe = dominfo.getElementsByTagName('GROUPE')[0].innerHTML;

// AFFICHAGES INFOS FICHIER
var idfichier = dominfo.getElementsByTagName('DOCUMENT')[0].id;
document.getElementById('infotraduction').innerHTML = idfichier;

// AFFICHAGE DU NOMBRE TOTAL DE PHRASES
var totalp = doms.getElementsByTagName('P').length;
console.log("Longueur totale :", totalp);
document.getElementById('ptotal').innerHTML = totalp-1;


// NAVIGATION PHRASE
var currentp = 0;
show(0);

function previous(){
    if(currentp>0){
        currentp -= 1;
        console.log('Previous ',currentp+1, '→', currentp);
        show(currentp);
    }
}

function next(){
    if(currentp<totalp){
        currentp += 1;
        console.log('Next ',currentp-1, '→', currentp);
        show(currentp);
    }
}

// SAUTER DIRECTEMENT A LA PHRASE INDIQUEE
function gotoP(){
    var pid = document.getElementById('pid').value;
    currentp = Number(pid);
    console.log('goTo ',currentp);
    show(currentp);
}
document.getElementById('pid').addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        event.preventDefault(); // Cancel the default action, if needed
        gotoP();
    }
});

// PHRASE SUR UNE SEULE LIGNE
function toggleWordWrap(){
    if (document.getElementById('wordwrap').checked){
        // on décoche
        document.getElementById('maintable').style.width = "100%";
        document.getElementById('wordwrap').checked = false;
        connectAll();
    } else {
        // on coche
        document.getElementById('maintable').style.width = "max-content";
        document.getElementById('wordwrap').checked = true;
        connectAll();
    }
}


// AFFICHER LA PHRASE CIBLE
function show(pid){
    document.getElementById('targs').innerHTML = doms.getElementById('p'+currentp).innerHTML;
    document.getElementById('targi').innerHTML = domi.getElementById('p'+currentp).innerHTML;
    document.getElementById('targr').innerHTML = domr.getElementById('p'+currentp).innerHTML;
    document.getElementById('targf').innerHTML = domf.getElementById('p'+currentp).innerHTML;
    document.getElementById('pid').value = pid;
    document.getElementById('infoparagraphe').innerHTML = doms.getElementById('p'+currentp).parentNode.id;
    document.getElementById('infophrase').innerHTML = 'p'+currentp;

    thisp = domr.getElementById('p'+currentp);

    var listspan = thisp.getElementsByTagName('span');
    document.getElementById('targreditSup').innerHTML = "";
    document.getElementById('targreditInf').innerHTML = "";
    mt = -20; // margin-top de la div annotation à incrémenter pour décaler vers le bas
    for (span=0; span<listspan.length; span++){
        mt+=30;
        if (listspan[span].classList != "suppression"){

            let textref = document.getElementById(listspan[span].id);

            newEdit = document.createElement('div');
            newEdit.classList = "divEdit align-self-center";
            newEdit.id = 'div'+listspan[span].id;
            newEdit.style.marginTop = mt + "px";

            newEditTitle = document.createElement('div');
            newEditTitle.classList = "divEditTitle";
            newEditTitle.innerHTML = listspan[span].getAttribute("aut");
            newEdit.appendChild(newEditTitle);

            newEditDate = document.createElement('div');
            newEditDate.classList = "divEditDate";
            newEditDate.innerHTML = listspan[span].getAttribute("date").replace("T"," ");
            newEdit.appendChild(newEditDate);

            // CONTENU DE L'ANNOTATION
            if (listspan[span].getAttribute("first-type")){
                // Cas du suivi de modif, suppression d'un ajout
                newEditFirstInsert = document.createElement('div');
                newEditFirstInsert.classList = "divEditFirstInsert";
                newEditFirstInsert.innerHTML = "inséré par " + listspan[span].getAttribute("first-aut") + "<br>" + listspan[span].getAttribute("first-date");
                newEdit.appendChild(newEditFirstInsert);
            } else if (listspan[span].classList == "commentaire") {
                // Si c'est un commentaire, on affiche simplement la valeur de text
                newEditText = document.createElement('div');
                newEditText.classList = "divEditText";
                newEditText.innerHTML = listspan[span].getAttribute("text");
                newEdit.appendChild(newEditText);
            } else if (listspan[span].classList == "suiviModif substitution") {
                // Si c'est un commentaire, on affiche simplement la valeur de text
                newEditText = document.createElement('div');
                newEditText.classList = "divEditText";
                newEditText.innerHTML = listspan[span].getAttribute("final");
                newEdit.appendChild(newEditText);
            }

            newEditId = document.createElement('div');
            newEditId.classList = "divEditId";
            newEditId.innerHTML = "id:"+listspan[span].getAttribute("id");
            newEdit.appendChild(newEditId);


            if (listspan[span].classList == "suiviModif deletion"){
                document.getElementById('targreditSup').appendChild(newEdit);
            } else {
                document.getElementById('targreditInf').appendChild(newEdit);
            }

            // Colorations
            if (listspan[span].classList == "suiviModif insertion"){
                textref.style.backgroundColor = colorAdd;
                document.getElementById('div'+listspan[span].id).style.borderColor = colorAdd;
            } else if (listspan[span].classList == "suiviModif deletion"){
                textref.style.backgroundColor = colorDel;
                document.getElementById('div'+listspan[span].id).style.borderColor = colorDel;
            } else if (listspan[span].classList == "suiviModif substitution"){
                textref.style.backgroundColor = colorSub;
                document.getElementById('div'+listspan[span].id).style.borderColor = colorSub;
            } else if (listspan[span].classList == "commentaire"){
                textref.style.backgroundColor = editcolor[span];
                document.getElementById('div'+listspan[span].id).style.borderColor = editcolor[span];
            }

            // HOVER : FOCUS
            newEdit.addEventListener("mouseover", function(el) {
                document.getElementById(this.id.replace('div','')).classList.add("textfocus");
                document.getElementById(this.id.replace('div','path')).classList.add("pathfocus");
                document.getElementById(this.id).classList.add("divfocus");
            } )
            newEdit.addEventListener("mouseout", function(el) {
                document.getElementById(this.id.replace('div','')).classList.remove("textfocus");
                document.getElementById(this.id.replace('div','path')).classList.remove("pathfocus");
                document.getElementById(this.id).classList.remove("divfocus");
            } )

            textref.addEventListener("mouseover", function(el) {
                this.classList.add("textfocus");
                document.getElementById('path'+this.id).classList.add("pathfocus");
                document.getElementById('div'+this.id).classList.add("divfocus");
            } )
            textref.addEventListener("mouseout", function(el) {
                this.classList.remove("textfocus");
                document.getElementById('path'+this.id).classList.remove("pathfocus");
                document.getElementById('div'+this.id).classList.remove("divfocus");
            } )

            // CLICK : EDIT
            newEdit.addEventListener("click", function() {
                document.querySelectorAll('.divclick').forEach((el) => {el.classList.remove('divclick')});
                this.classList.add('divclick');
                let i=0;
                while (i<150) {
                    setTimeout(function() {
                        connectAll();
                    }, 100);
                    i++;
                }
            })

            // Make it movable
            // dragElement(newEdit);
        }
    }
    connectAll();
    getElan();
}

document.addEventListener('click', (evt) => {
    document.querySelectorAll('.divclick').forEach((el) => {
        if (!el.contains(evt.target)) {
            el.classList.remove('divclick');
            let i=0;
            while (i<150) {
                setTimeout(function() {
                    connectAll();
                }, 100);
                i++;
            }
        }
    });
});

// Make the DIV element draggable:
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
      /* if present, the header is where you move the DIV from:*/
      document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
      /* otherwise, move the DIV from anywhere inside the DIV:*/
      elmnt.onmousedown = dragMouseDown;
    }
  
    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }
  
    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      elmnt.style.top = e.clientY - 50 + "px";
      elmnt.style.left = e.clientX - 50 + "px";
      connectAll();
    }
  
    function closeDragElement() {
      /* stop moving when mouse button is released:*/
      document.onmouseup = null;
      document.onmousemove = null;
    }
  }



// RÉCUPÉRATION ELAN
async function getElan() {
    var colis = {
        'trdId': idfichier,
        'p': currentp
    }
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/getElan/', options);
    const data = await response.json();
    console.log(data);

    showElan(data)
}


// Lister les locuteurs présents et leur attribuer une classe de style fixe codeEtu→etu1)
let etu2style = {};
let n = 0;
for (e of etus.traducteurs) n++; etu2style[e] = "etu"+n;
for (e of etus.correcteurs) n++; etu2style[e] = "etu"+n;

document.querySelectorAll('.badgEtu').forEach( (el) => el.classList.add(etu2style[el.innerText]));

function showElan(data){
    let tables = document.getElementById('tables');
    let videos = document.getElementById('videos');

    tables.innerHTML = "";
    videos.innerHTML = "";
    
    for (d=0; d<data['segs'].length; d++) {
        let targetVideo = "vid"+d;
        let offset = data['segs'][d]['offset'];

        // Table ELAN
        let newTable = `
        <table class="table table-responsive table-dark d-flex flex-column" style="max-height: 80vh; width:100%">
                <tbody>
                  <tr>
                    <th scope="col">Étudiant</th>
                    <th scope="col" style="width:100%">Transcription</th>
                    <th scope="col">Début&nbsp;(s)</th>
                    <th scope="col">Fin&nbsp;(s)</th>
                  </tr>
        `;

        for (i=0; i<data['segs'][d]['pints'].length; i++){
            let etu = data['segs'][d]['pints'][i][0];
            let langue = data['segs'][d]['pints'][i][1];
            let tmin = data['segs'][d]['pints'][i][2];
            let tmax = data['segs'][d]['pints'][i][3];
            let txt = data['segs'][d]['pints'][i][4];
            let jp2fr = data['segs'][d]['pints'][i][5];
            
            newTable += `<tr class="table-active seg" onclick="playSeg(${tmin},${offset},'${targetVideo}')">
                            <th scope="row">${etu}</th>
                            <td class="d-flex flex-column">
                                <div class="${ etu2style[etu] } ${ langue }">${txt}</div>
                                <div class="${ etu2style[etu] } tier-jp2fr">${jp2fr}</div>
                            </td>
                            <td class="tier-tmps">${tmin}</td>
                            <td class="tier-tmps">${tmax}</td>
                        </tr>`;
        }
        newTable += '</tbody></table>';
        tables.innerHTML += newTable;

        //// VIDEO
        videos.innerHTML += `<video id="${targetVideo}" class="videos mt-4" width="100%" controls ontimeupdate="colorSeg(this, ${offset})">
            <source src="/static/corpus/zoom/${ groupe }/${ data['segs'][d]['video'] }" type="video/mp4" /> 
            Problème de format avec la vidéo...
        </video>`;
    }

    if (videos.innerHTML == "") videos.innerHTML = "<div style='width:100%;text-align:center'>Pas de discussion vidéo associée pour cette phrase.</div>";
}

function playSeg(min, offset, targetVideo){
    // play untill max : https://www.codespeedy.com/play-selected-part-of-a-video-in-javascript/
    // offset : nb seconde à soustraire (comme la vidéo est coupée à la durée de p (moins 3 secondes))

    document.querySelectorAll('.videos').forEach((el) => { el.pause(); });

    targetTime = min-offset;
    let vid = document.getElementById(targetVideo);
    vid.currentTime = targetTime;
    vid.play();
}


function colorSeg(vid,offset){
    let segs = document.querySelectorAll('.seg');
    let ct = vid.currentTime;

    for(i=0; i<segs.length; i++) {
        if (parseFloat(segs[i].children[2].innerText)-offset<=ct && parseFloat(segs[i].children[3].innerText)-offset>=ct){
            segs[i].classList.add('segfocus');
        }
        else{
            segs[i].classList.remove('segfocus');
        }
    }
    if (ct > parseFloat(segs[segs.length-1].children[3].innerText)) vid.pause()
}