"""actid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.contrib.auth import views as auth_views
from corpolan import views as corpolan_views

from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', corpolan_views.loginView, name='login'), #auth_views.LoginView.as_view(template_name="users/login.html"), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="users/logout.html"), name='logout'),
    path('', corpolan_views.home, name='home'),
    
    path('suppression/<str:ClassName>/<str:instId>/', corpolan_views.delInstancePage, name='delInstancePage'),
    path('deleteInstance/<str:ClassName>/<str:instId>/', corpolan_views.delInstance, name='delInstance'),
    
    path('etudiants/', corpolan_views.etudiants, name='etudiants'),
    path('showEtudiant/<str:etuId>/', corpolan_views.showEtudiant, name='showEtudiant'),
    path('addEtudiant/', corpolan_views.addEtudiant, name='addEtudiant'),
    path('editEtudiant/<str:etuId>/', corpolan_views.editEtudiant, name='editEtudiant'),
    
    path('groupes/', corpolan_views.groupes, name='groupes'),
    path('showGroupe/<str:grpId>/', corpolan_views.showGroupe, name='showGroupe'),
    path('addGroupe/', corpolan_views.addGroupe, name='addGroupe'),
    path('editGroupe/<str:grpId>/', corpolan_views.editGroupe, name='editGroupe'),

    path('traductions/', corpolan_views.traductions, name='traductions'),
    path('showTraduction/<str:trdId>/', corpolan_views.showTraduction, name='showTraduction'),
    path('addTraduction/', corpolan_views.addTraduction, name='addTraduction'),
    path('editTraduction/<str:trdId>/', corpolan_views.editTraduction, name='editTraduction'),

    path('corpusViewer/<str:trdId>/', corpolan_views.corpusViewer, name='corpusViewer'),
    path('getElan/', csrf_exempt(corpolan_views.getElan), name='getElan'),

    path('recherche/', corpolan_views.recherche, name='recherche'),
]
